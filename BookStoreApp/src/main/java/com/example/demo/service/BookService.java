package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Book;
import com.example.demo.repository.BookRepository;

import java.util.List;

@Service
public class BookService {
	
	@Autowired
	private BookRepository repository;
	
	public Book addBook(Book book) {
		return repository.save(book);
	}
	
	public List<Book> addBooks(List<Book> books) {
		return repository.saveAll(books);
	}
	
	public List<Book> getBooks(){
		return repository.findAll();
	}
	
	public Book getSingleBook(int id){
		return repository.findById(id).orElse(null);
	}
	
	
	public String deleteBook(int id) {
		 repository.deleteById(id);
		 return "Book Deleted"+id;
	}
	
	public Book updateBook(Book book) {
		Book existingBook  = repository.findById(book.getId()).orElse(null);
		existingBook.setTitle(book.getTitle());
		existingBook.setAuthorName(book.getAuthorName());
		return repository.save(existingBook);
		
	}
	
	

}
