package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Book;
import com.example.demo.service.BookService;


@RestController
@CrossOrigin(origins="*")
public class BookController {
	
	
	@Autowired
	private BookService service;
	

	@GetMapping("/allbooks")
	public List<Book> findAllBooks(){
		return service.getBooks();
	}
	
	@PostMapping("/addBook")
	public Book addBook(@RequestBody Book book) {
		return service.addBook(book);
	}
	

	@PostMapping("/addBooks")
	public List<Book> addBooks(@RequestBody List<Book> books) {
		return service.addBooks(books);
	}
	
	

	@GetMapping("/singleBook/{id}")
	public Book findBookById(@PathVariable int id){
		return service.getSingleBook(id);
	}
	
	

	@PutMapping("/updateBook")
	public Book updateBook(@RequestBody Book book) {
		return service.updateBook(book);
	}
	
	
	@DeleteMapping("deleteBook/{id}")
	public String deleteBook(@PathVariable int id){
		return service.deleteBook(id);
	}
}